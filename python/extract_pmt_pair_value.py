#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2018 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

from gnuradio import gr
import pmt

class extract_pmt_pair_value(gr.basic_block):
    """
    docstring for block extract_pmt_pair_value
    """
    def __init__(self):
        gr.basic_block.__init__(self,
            name="extract_pmt_pair_value",
            in_sig=None,
            out_sig=None)
	self.message_port_register_in(pmt.intern('msg_in'))
	self.message_port_register_out(pmt.intern('msg_out'))
	self.set_msg_handler(pmt.intern("msg_in"), self.handle_msg)

    def forecast(self, noutput_items, ninput_items_required):
        #setup size of input_items[i] for work call
        for i in range(len(ninput_items_required)):
            ninput_items_required[i] = noutput_items

    def handle_msg(self, msg):
	if pmt.is_uniform_vector(pmt.cdr(msg)):
	    self.message_port_pub(pmt.intern('msg_out'), pmt.cdr(msg))
		
