INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_BEESAT_SDR_TNC_MSG_HANDLER beesat_sdr_tnc_msg_handler)

FIND_PATH(
    BEESAT_SDR_TNC_MSG_HANDLER_INCLUDE_DIRS
    NAMES beesat_sdr_tnc_msg_handler/api.h
    HINTS $ENV{BEESAT_SDR_TNC_MSG_HANDLER_DIR}/include
        ${PC_BEESAT_SDR_TNC_MSG_HANDLER_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    BEESAT_SDR_TNC_MSG_HANDLER_LIBRARIES
    NAMES gnuradio-beesat_sdr_tnc_msg_handler
    HINTS $ENV{BEESAT_SDR_TNC_MSG_HANDLER_DIR}/lib
        ${PC_BEESAT_SDR_TNC_MSG_HANDLER_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(BEESAT_SDR_TNC_MSG_HANDLER DEFAULT_MSG BEESAT_SDR_TNC_MSG_HANDLER_LIBRARIES BEESAT_SDR_TNC_MSG_HANDLER_INCLUDE_DIRS)
MARK_AS_ADVANCED(BEESAT_SDR_TNC_MSG_HANDLER_LIBRARIES BEESAT_SDR_TNC_MSG_HANDLER_INCLUDE_DIRS)

